import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = () =>
  new Vuex.Store({
    state: {
      showModalLogin: false,
      showModalRegister: false,
      showBuyModal: false,
      game: '',
      isAdmin: true,
      localId: '',
      allGames: [],
      idToken: '',
    },
    mutations: {
      TOGGLE_MODAL(state, payload) {
        if (payload.login) {
          state.showModalLogin = !state.showModalLogin;
        } else if (payload.registration) {
          state.showModalRegister = !state.showModalRegister;
        } else if (payload.buyGame) {
          state.showBuyModal = !state.showBuyModal;
          state.game = payload.game;
        } else {
          state.showModalRegister = false;
          state.showBuyModal = false;
        }
      },
      AUTH_USER(state, payload) {
        state.idToken = payload.idToken;
        state.localId = payload.localId;
        console.log(state);
      }
    },
    actions: {
      login({ commit }, payload) {
        console.log(payload);
      }
    }
  });

export default store;
